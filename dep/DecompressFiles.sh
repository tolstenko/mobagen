#!/bin/bash
7z.exe x mono-4.2.1.102.tar.bz2
7z.exe x mono-4.2.1.102.tar
rename mono-4.2.1 mono

7z.exe x ogre_src_v1-8-1.exe
rename ogre_src_v1-8-1 ogre

7z.exe x boost_1_59_0.7z
rename boost_1_59_0 boost

7z.exe x ogredep.zip
rename cabalistic-ogredeps-eb18d4651ec7 ogredep

PATH=C:\cygwin\bin;$PATH
%bash --login -i -c ./configure --prefix=bin --disable-nls --host=i686-pc-mingw32
%bash ./configure --prefix=`pwd`/bin --disable-nls --host=i686-pc-mingw32
set /p DUMMY=Hit ENTER to continue...