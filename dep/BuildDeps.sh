#!/bin/bash
mkdir bin
PREFIX=`pwd`/bin
cd mono ; ./configure --prefix=$PREFIX --disable-nls --host=i686-pc-mingw32
cd mono ; make -j 9
cd mono ; make install
