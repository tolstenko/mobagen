#!/bin/bash
cd ..
git submodule add -b mono-4.4.0-branch https://github.com/mono/mono.git dep/mono
git submodule add -b https://github.com/mono/monodevelop.git /dep/monodevelop
cd dep/mono
./autogen.sh --prefix=`pwd`/build --disable-nls
make get-monolite-latest
make -j`sysctl -n hw.ncpu` && make install
