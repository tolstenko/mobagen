setup-x86.exe --root "C:\cygwin" --quiet-mode --packages libboost-devel,cmake,cmake-gui,git,autoconf,automake,bison,gcc-core,gcc-g++,mingw-runtime,mingw-binutils,mingw-gcc-core,mingw-gcc-g++,mingw-pthreads,mingw-w32api,libtool,make,python,gettext-devel,gettext,intltool,libiconv,pkg-config,git,curl,libxslt
powershell -Command "(New-Object Net.WebClient).DownloadFile('http://download.mono-project.com/sources/mono/mono-4.2.1.102.tar.bz2', 'mono-4.2.1.102.tar.bz2')"
powershell -Command "(New-Object Net.WebClient).DownloadFile('http://nbtelecom.dl.sourceforge.net/project/ogre/ogre/1.8/1.8.1/ogre_src_v1-8-1.exe', 'ogre_src_v1-8-1.exe')"
powershell -Command "(New-Object Net.WebClient).DownloadFile('http://nbtelecom.dl.sourceforge.net/project/boost/boost/1.59.0/boost_1_59_0.7z', 'boost_1_59_0.7z')"
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://bitbucket.org/cabalistic/ogredeps/get/eb18d4651ec7.zip', 'ogredep.zip')"

set /p DUMMY=Hit ENTER to continue...